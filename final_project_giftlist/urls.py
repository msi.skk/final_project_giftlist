"""final_project_giftlist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from gift.views import (
    AboutView,
    login_form_view,
    logout_view,
    # user_card,
    RegisterCreateView,
    UserPageView,
    register_confirmation
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("gift.urls")),
    # path("profile/", user_card, name="profile"),
    path("profile/", UserPageView.as_view(), name="profile"),
    path("login/", login_form_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("register/", RegisterCreateView.as_view(), name="register"),
    path("confirmation/<uuid:uid>", register_confirmation, name="confirmation"),
    path("about/", AboutView.as_view(), name="about"),
    path("", AboutView.as_view(), name="index"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

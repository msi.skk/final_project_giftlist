from enum import IntEnum


class EventType(IntEnum):
    birthsday = 10
    nameday = 20
    anniversary = 30
    feast = 40

from django.contrib import (
    messages,
)  # jednorázové správy (info, success, warning, error, debug)
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import (
    CreateView,
    DeleteView,
    ListView,
    TemplateView,
    UpdateView,
    DetailView,
)

from gift.forms import GiftForm, PersonForm, EventForm, RegisterForm
from gift.models import (
    Event,
    EventType,
    Gift,
    Person,
    Relationship,
    RelationshipRequest,
)


class GiftView(LoginRequiredMixin, ListView):
    template_name = "gift_grid.html"
    model = Gift
    # queryset = Gift.objects.filter(created_by_id=2)

    def get_queryset(self):
        queryset = super(GiftView, self).get_queryset()
        queryset = queryset.filter(created_by=self.request.user)
        return queryset


class PersonView(LoginRequiredMixin, ListView):
    template_name = "person_grid.html"
    model = Person

    def get_queryset(self):
        queryset = super(PersonView, self).get_queryset()
        queryset = queryset.filter(created_by=self.request.user)
        return queryset


class EventView(LoginRequiredMixin, ListView):
    template_name = "event_grid.html"
    model = Event

    def get_queryset(self):
        queryset = super(EventView, self).get_queryset()
        queryset = queryset.filter(person__created_by=self.request.user)
        return queryset.order_by("date")


class UserPageView(LoginRequiredMixin, ListView):
    template_name = "user_page.html"
    model = Gift

    def get_queryset(self):
        queryset = super(UserPageView, self).get_queryset()
        # queryset = queryset.filter(gift__created_by=self.request.user)
        queryset = queryset.filter(person__related_user=self.request.user)
        return queryset

# def user_card(request):
#     if not request.user.is_authenticated:
#         return redirect("login")
#     return render(request, template_name="user_page.html")


@login_required
def person_card(request, pk):
    record = Person.objects.get(id=pk)
    context = {"person": record}
    return render(request, "person_page.html", context)


class GiftPageView(DetailView):
    model = Gift
    template_name = "gift_page.html"


class AboutView(TemplateView):
    # page = 'about'
    template_name = "about.html"


def login_form_view(request):
    if request.user.is_authenticated:
        messages.info(request, "You are already logged in as " + request.user.username)
        return redirect("profile")

    if request.method == "POST":
        user_name = request.POST["username"].lower()
        pwd = request.POST["password"]

        try:
            user = User.objects.get(username=user_name)
        except:
            messages.error(request, "Username does not exist")

        user = authenticate(request, username=user_name, password=pwd)
        if user is not None:
            login(request, user)
            messages.success(request, "You are succefully logged in.")
            return redirect("gift:gifts")
        else:
            messages.error(request, "Username or password is incorrect")

    return render(request, "login_form.html")


def logout_view(request):
    logout(request)
    return redirect("login")


def register_confirmation(request, uid):
    try:
        search = Person.objects.get(uuid_id=uid)
    except ObjectDoesNotExist:
        search = False
        messages.error(request, f"invalid activation link")

    if search:
        print("is person")
        confirmed_user = User.objects.get(id=search.related_user.id)
        if confirmed_user.is_active:
            messages.warning(request, "Your account was already activated.")
        else:
            confirmed_user.is_active = True
            confirmed_user.save()
            # POZOR pri deaktivovaní - treba zmeniť uuid viazanej osoby, aby
            # užívateľ nemohol opätovne použiť linku z mailu
            # person.uuid_id = uuid4()
            # person.save()

            messages.success(request, "Your account is now active.")

    return HttpResponseRedirect(reverse("login"))


# CREATE VIEWS
class RegisterCreateView(CreateView):
    template_name = 'register_form.html'
    form_class = RegisterForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if form.is_valid():
            form.save()
            messages.success(self.request, "Registration succeeded! Confirmation mail was sent.")
        else:
            messages.warning(self.request, "Registration failed!")
        return redirect('login')

    def get_object(self, queryset=None):
        return self.request.user

    def get_initial(self):
        results = super().get_initial()
        results["person"] = Person()
        # results["permanent_address"] = self.request.user.profile.permanent_address
        return results



# @login_required
# def GiftCreateView(request):
#     record = request.POST
#     form = GiftForm(record, request.FILES)
#     form.instance.created_by = request.user
#     messages.info(request, str(form.instance.picture))
#
#     if form.is_valid():
#         form.save()
#         messages.success(request, "New gift was created")
#         return redirect("gift:gifts")
#
#     context = {"form": form, "picture_url": form.instance.picture.url}
#     return render(request, "gift_form.html", context)
#
#     # def form_valid(self, form):
#     #     form.instance.created_by = self.request.user
#     #     return super().form_valid(form)


@login_required
def gift_create_view(request):
    if request.method == 'GET':
        form = GiftForm()
        form.fields['person'].queryset = Person.objects.filter(
            created_by=request.user)
        form.fields['event'].queryset = Event.objects.filter(
            person__created_by=request.user)

        context = {'form': form}
        return render(request, "gift_form.html", context)

    elif request.method == 'POST':
        form = GiftForm(data=request.POST, files=request.FILES)
        form.instance.created_by = request.user
        context = {'form': form}

        if form.is_valid():
            form.save()
            messages.success(request, "New gift was created")
            return redirect('gift:gifts')
        else:
            return render(request, "gift_form.html", context)

    return redirect('gift:gifts')


@login_required
def person_create_view(request):
    record = request.POST
    form = PersonForm(record, request.FILES)
    form.instance.created_by = request.user
    messages.info(request, str(form.instance.picture))

    if form.is_valid():
        form.save()
        messages.success(request, "New person was created")
        return redirect("gift:persons")

    context = {"form": form, "picture_url": form.instance.picture.url}
    return render(request, "person_form.html", context)


class EventCreateView(LoginRequiredMixin, CreateView):
    template_name = 'form.html'
    form_class = EventForm
    success_url = reverse_lazy('gift:events')

    def get_initial(self):
        output = super().get_initial()
        output["person"] = Person.objects.get(id=self.request.GET["person_id"])
        return output

    # def get_form_kwargs(self):
    #     kwargs = super().get_form_kwargs()
    #     kwargs['person'] = Person.objects.get(id=pk)
    #     return kwargs


# UPDATE VIEWS
@login_required
def gift_update_view(request, pk):
    record = Gift.objects.get(id=pk)
    form = GiftForm(instance=record)

    if request.method == 'GET':
        form.fields['person'].queryset = Person.objects.filter(
            created_by=request.user)
        form.fields['event'].queryset = Event.objects.filter(
            person__created_by=request.user)

    if request.method == "POST":
        form = GiftForm(request.POST, request.FILES, instance=record)
        if form.is_valid():
            form.save()
            messages.success(request, "Gift changes was saved")
            return redirect("gift:gifts")
        else:
            messages.error(request, 'Error saving form')

    context = {"form": form, "pk": pk, "picture_url": record.picture.url}
    return render(request, "gift_form.html", context)


@login_required
def person_update_view(request, pk):
    record = Person.objects.get(id=pk)
    form = PersonForm(instance=record)

    if request.method == "POST":
        form = PersonForm(request.POST, request.FILES, instance=record)
        if form.is_valid():
            form.save()
            messages.success(request, "Gift changes was saved")
            return redirect("gift:persons")

    context = {"form": form, "pk": pk, "picture_url": record.picture.url, "can_delete": record.deletable}
    return render(request, "person_form.html", context)


class EventUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'form.html'
    model = Event
    form_class = EventForm
    success_url = reverse_lazy('gift:events')

    # def get_context_data(self):
    #     return

    # def get_queryset(self):
    #     queryset = super(EventUpdateView, self).get_queryset()
    #     form.fields['event'].queryset = Event.objects.filter(
    #         person__created_by=request.user)
    #     event.queryset = queryset.filter(person__created_by=self.request.user)
    #     queryset['event_type'] = queryset['event_type'].filter()
    #     return queryset


# DELETE VIEWS
@login_required
def DeleteRecordView(request, pk):
    record = Gift.objects.get(id=pk)

    if request.method == "POST":
        form = GiftForm(request.POST, instance=record)
        if form.is_valid():
            record.delete()
            messages.warning(request, "Gift was deleted")
            return redirect("gift:gifts")

    context = {"object": record}
    return render(request, "delete_confirmation.html", context)


@login_required
def DeletePersonView(request, pk):
    record = Person.objects.get(id=pk)

    if request.method == "POST":
        form = PersonForm(request.POST, instance=record)
        if form.is_valid():
            record.delete()
            # redirect_page = "gift:persons"
            redirect_page = request.path
            print(redirect_page)
            messages.warning(request, "Person was deleted")
            return redirect(redirect_page)

    context = {"object": record}

    return render(request, "delete_confirmation.html", context)


class DeleteEventView(LoginRequiredMixin, DeleteView):
    Model = Event
    success_url = reverse_lazy('gift:events')

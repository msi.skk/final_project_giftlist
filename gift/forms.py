import colorfield
from colorfield import widgets
from colorfield.fields import ColorField
from colorfield.widgets import ColorWidget
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

# from django.db.models import TextField
from django.db.transaction import atomic
from django.forms import (
    CharField,
    ChoiceField,
    DateField,
    EmailField,
    HiddenInput, ImageField,
    ModelChoiceField,
    ModelForm,
    Textarea,
)

from gift.choices import COLOR_CHOICES
from gift.models import (
    Event,
    EventType,
    Gift,
    Person,
    Relationship,
    RelationshipRequest,
)
#email sending
from django.core.mail import send_mail
from django.conf import settings

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        # fields = '__all__'
        fields = ["username", "email", "password1", "password2"]

    username = CharField(label="Uživatelské jméno", max_length=32, required=False)
    email = CharField(label="E-mail", max_length=128, required=True)
    first_name = CharField(label="Jméno", max_length=32, required=False)
    last_name = CharField(label="Příjmení", max_length=32, required=False)
    color = ColorField(choices=COLOR_CHOICES)
    about = CharField(
        label="Something about you",
        widget=Textarea,
        min_length=42,
        required=False
    )

    @atomic
    def save(self, commit=True):
        # INSERT INTO auth_user ...
        result = super().save(commit=False)
        result.is_active = False
        result = super().save(commit=True)

        person = Person(
            related_user=result,
            created_by=result,
            nickname="Já",
            #biography=self.cleaned_data["biography"],
        )
        confirmation_link = "http://localhost:8100/confirmation/" + str(person.uuid_id)
        email_subject = 'Confirm your registration'
        email_message = f'''Thank you for you registration
        Your new accout: {result.username}
        
        Please confirm activation of the account using this link:
        {confirmation_link}
        '''

        if commit:
            # INSERT INTO gift_person ...
            person.save()
            # send e-mail with confirmation link
            send_mail(
                email_subject,
                email_message,
                "confirm@slancik.eu",
                [result.email],
                fail_silently=False,
            )

        return result

class GiftForm(ModelForm):
    class Meta:
        model = Gift
        fields = '__all__'
        # fields = ['event', 'picture', 'color']
        exclude = ['created_by']

    # person = ModelChoiceField(
    #     queryset=Person.objects.filter(created_by=1), required=False
    # )
    # event = ModelChoiceField(
    #     queryset=Event.objects.filter(person__created_by=1), required=False
    # )
    person = ModelChoiceField(queryset=Person.objects, required=False)
    event = ModelChoiceField(queryset=Event.objects, required=False)
    gift_name = CharField(
        max_length=32,
        required=True
    )
    picture = ImageField(
        label='Picture',
        max_length=128,
        required=False
    )
    description = CharField(
        # default='popis',
        widget=Textarea,
        label='Popis',
        required=False,
    )
    color = ColorField(choices=COLOR_CHOICES, blank=True, editable=False)

    def clean_web_link(self):
        cleaned=self.cleaned_data['web_link']
        if cleaned:
            cleaned.lower()
            urlhead = cleaned.split("//", 1)
            if not urlhead in ["http", "https"]:
                cleaned.join("http://")
        return cleaned

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = "__all__"
        exclude = ["created_by", "related_user"]
        # hidden = []
        # context = {'related_user_id': Person.related_user.id}


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = "__all__"
        exclude = []

    person = ModelChoiceField(
        queryset=Person.objects, required=True, show_hidden_initial=True,
        widget=HiddenInput(),
    )
    event_type = ModelChoiceField(queryset=EventType.objects, required=True)
    date = DateField(required=True)

from django.contrib import admin

from .models import Event, EventType, Gift, Person, Relationship, RelationshipRequest

admin.site.register(Gift)
admin.site.register(Event)
admin.site.register(EventType)
admin.site.register(Person)
admin.site.register(Relationship)
admin.site.register(RelationshipRequest)


# Register your models here.

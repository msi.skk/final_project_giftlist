from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from gift.views import (
    DeleteRecordView,
    EventView,
    gift_create_view,
    gift_update_view,
    GiftView,
    person_create_view,
    person_update_view,
    PersonView,
    person_card, DeleteEventView, EventUpdateView, EventCreateView, GiftPageView,
)

app_name = "gift"
urlpatterns = [
    path("gifts/", GiftView.as_view(), name="gifts"),
    path("gift/<str:pk>/", GiftPageView.as_view(), name="gift"),
    path("gift/create/", gift_create_view, name="gift_create"),
    path("gift/update/<str:pk>/", gift_update_view, name="gift_update"),
    path("gift/delete/<str:pk>/", DeleteRecordView, name="gift_delete"),

    path("persons/", PersonView.as_view(), name="persons"),
    path("person/<str:pk>", person_card, name="person"),
    path("person/create/", person_create_view, name="person_create"),
    path("person/delete/<str:pk>/", DeleteRecordView, name="person_delete"),
    path("person/update/<str:pk>/", person_update_view, name="person_update"),
    #
    path("events", EventView.as_view(), name="events"),
    # path("event/<str:pk>/", EventPageView.as_view(), name="event"),
    path('event/create/', EventCreateView.as_view(), name='event_create'),
    path('event/update/<str:pk>/', EventUpdateView.as_view(), name='event_update'),
    path('event/delete/<str:pk>/', DeleteEventView.as_view(), name='event_delete'),
    #
    # path('profile/int:uid/', ProfileView.as_view(), name='profile'),
    # path('wishes/int:gid/', WishesView.as_view(), name='wishes'),
]

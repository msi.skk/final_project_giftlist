from uuid import uuid4

from colorfield.fields import ColorField, ColorWidget
from django.contrib.auth.models import User
from django.db.models import (
    CASCADE,
    PROTECT,
    SET_DEFAULT,
    SET_NULL,
    BooleanField,
    CharField,
    DateField,
    ForeignKey,
    ImageField,
    Model,
    SmallIntegerField,
    TextField,
    URLField,
    UUIDField,
)

# from gift.choices import EVENT_TYPES
from gift.choices import COLOR_CHOICES
from gift.utils.storage import upload_to_user_dir

from datetime import date

DEFAULT_PIC_PERSON = "default_person.gif"
DEFAULT_PIC_GIFT = "default_gift.gif"


def make_label(long_name, label_max_length, continue_string) -> str:
    """ shorten given long_name if longer than label_max_length
    using continue_string instead of end part. """

    return long_name[:label_max_length - len(continue_string)] + ".." if len(long_name) > label_max_length else long_name

class Person(Model):
    created_by = ForeignKey(User, null=False, on_delete=PROTECT, related_name="persons")
    related_user = ForeignKey(
        User, null=True, blank=True, on_delete=SET_NULL, related_name="personprofile"
    )  # voliteľná väzba na užívateľa
    nickname = CharField(max_length=128, null=False, blank=False)
    picture = ImageField(
        null=True,
        blank=True,
        upload_to=upload_to_user_dir,
        default=DEFAULT_PIC_PERSON,
    )
    color = ColorField(max_length=7, default="#FFFFFF", choices=COLOR_CHOICES)
    about = TextField(default="", null=False, blank=True)
    uuid_id = UUIDField(default=uuid4, unique=True, editable=False)

    def __str__(self):
        return self.nickname + " | by " + self.created_by.username

    def set_picture_to_default(self):
        self.picture.delete(save=False)
        self.picture = DEFAULT_PIC_PERSON
        self.save()

    @property
    def name_label(self):
        return make_label(self.nickname.capitalize(), 16, "..")

    @property
    def deletable(self):
        if self.related_user:
            return False
        else:
            return True

class EventType(Model):
    event_type_name = CharField(max_length=64, null=False, blank=False)
    description = TextField(default="", null=False, blank=True)
    color = ColorField(max_length=7, default="#FFFFFF", choices=COLOR_CHOICES)
    recurrent = BooleanField(default=False)  # True ak sa udalosť opakuje každý rok

    def __str__(self):
        return self.event_type_name


class Event(Model):
    person = ForeignKey(
        Person, null=False, on_delete=CASCADE, related_name="events"
    )  # osoba na ktorú sa vzťahuje udalosť
    event_type = ForeignKey(
        EventType,
        null=False,
        default=1,
        on_delete=SET_DEFAULT,
        related_name="event_types",
    )  # druh udalosti
    # event_name = CharField(max_length=64, null=False, blank=False)
    # event_type = SmallIntegerField(
    #     choices=EVENT_TYPES,
    #     null=False,
    #     blank=False
    # )
    date = DateField(auto_now=False, auto_now_add=False, null=False, blank=False)
    # recurrent = BooleanField(default=False) # True ak sa udalosť opakuje každý rok

    def __str__(self):
        return (
            self.person.nickname
            + " "
            + self.event_type.event_type_name
            + " "
            + str(self.date)
        )

    @property
    def days_from_now(self)->int:
        today_days = date.today()
        date_days = self.date
        return int((date_days-today_days).days)


class Gift(Model):
    created_by = ForeignKey(
        User, null=False, on_delete=PROTECT, related_name="gifts"
    )  # užívateľ, ktorý gift založí
    event = ForeignKey(
        Event, null=True, blank=True, on_delete=SET_NULL, related_name="giftevent"
    )  # voliteľné priradenie k udalosti
    person = ForeignKey(
        Person, null=True, blank=True, on_delete=SET_NULL, related_name="giftperson"
    )  # voliteľné priradenie k osobe
    gift_name = CharField(max_length=64, null=False, blank=False)
    picture = ImageField(
        null=True, blank=True, upload_to=upload_to_user_dir, default=DEFAULT_PIC_GIFT
    )
    description = TextField(default="", null=False, blank=True)
    color = ColorField(max_length=7, default="#FFFFFF", choices=COLOR_CHOICES)
    web_link = URLField(
        default="",
        null=True,
        blank=True,
    )
    wish_text = TextField(default="", null=False, blank=True)
    hide = BooleanField(default=False)
    done = BooleanField(default=False)
    # for_me = BooleanField(default=False, null=False) # True označí že ide o vlasné prianie užívateľa (gift->wish)

    @property
    def for_me(self):
        return self.created_by == self.person.related_user

    @property
    def name_label(self):
        return make_label(self.gift_name, 16, "..")

    def __str__(self):
        return self.gift_name + " | by " + self.created_by.username

    def set_picture_to_default(self):
        self.picture.delete(save=False)
        self.picture = DEFAULT_PIC_GIFT
        self.save()


class Relationship(Model):
    user = ForeignKey(User, null=False, on_delete=PROTECT, related_name="relatives")
    related_users = ForeignKey(
        User,
        null=False,
        on_delete=PROTECT,
    )
    accepted = BooleanField(default=False)

    def __str__(self):
        return self.user.username + " <--> " + self.related_users.username


class RelationshipRequest(Model):
    relationship = ForeignKey(
        Relationship, null=False, on_delete=PROTECT, related_name="pending_requests"
    )

    def __str__(self):
        return (
            self.relationship.user.username
            + " --> "
            + self.relationship.related_users.username
        )

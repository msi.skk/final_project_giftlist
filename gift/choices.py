from gift.enums import EventType

EVENT_TYPES = (
    (EventType.birthsday.value, EventType.birthsday.name),
    (EventType.nameday.value, EventType.nameday.name),
    (EventType.anniversary.value, EventType.anniversary.name),
    (EventType.feast.value, EventType.feast.name),
)

COLOR_CHOICES = [
    ("#FFFFFF", "white"),
    ("#c0c0c0", "lightgray"),
    ("#909090", "gray"),
    ("#484848", "antracit"),
    ("#8EE83D", "limrgreen"),
    # ("#5FE83D", "lightgreen"),
    ("#42E83D", "green"),
    ("#3DE892", "softgreen"),
    ("#3DE4E8", "cyan"),
    # ("#3DB4E8", "lightblue"),
    ("#3D98E8", "riverblue"),
    ("#3D3EE8", "blue"),
    ("#973DE8", "violet"),
    # ("#C73DE8", "darkmagenta"),
    ("#E43DE8", "magenta"),
    ("#E83D94", "darkpink"),
    ("#E8413D", "red"),
    # ("#E8713D", "brickred"),
    ("#E88E3D", "orange"),
    ("#E8E73D", "yellow"),
]

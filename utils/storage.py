import os
import uuid


def upload_to_user_dir(instance, filename):
    """
    Generates relative path from "MEDIA_ROOT"

    :param instance: MediaData object
    :param filename: file name of uploaded file
    :return: file system path
    """

    ext = os.path.splitext(filename)
    return os.path.join("user", str(instance.created_by.id), f"{uuid.uuid4().hex}{ext}")


# class MyStorage(FileSystemStorage):
#     def get_available_name(self, name, max_length=None):
#         dir_name, file = os.path.split(name)
#         _, ext = os.path.splitext(file)
#         return os.path.join(dir_name, f"{uuid.uuid4().hex}{ext}")